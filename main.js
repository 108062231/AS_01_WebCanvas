var canvas = document.getElementById("canvas");
canvas.width = window.innerWidth-120;
canvas.height = 500;

var context = canvas.getContext("2d");
var canvas_color = "white";
context.fillStyle = canvas_color;
context.fillRect(0, 0, canvas.width, canvas.height);

var draw_mode = "pen";
canvas.style.cursor = `url("pen.png"),auto`;
var draw_color = "black";
var is_drawing = false;
var draw_width;
var text = "";
var fontstyle = "新細明體";
var sizestyle = "60"

var stroke_begin_x = 0;
var stroke_begin_y = 0;
var recover;


var undo_array = [];
var redo_array = [];
var undo_index = -1;
var redo_index = -1;


canvas.addEventListener("mousedown", start, false);
canvas.addEventListener("mousemove", draw, false);
canvas.addEventListener("mouseup", stop, false);
canvas.addEventListener("mouseout", stop, false);


function start(event){
    stroke_begin_x = event.clientX - canvas.offsetLeft;
    stroke_begin_y = event.clientY - canvas.offsetTop;
    if(draw_mode!="txt"){
        is_drawing = true;
        context.beginPath();
        context.moveTo(event.clientX - canvas.offsetLeft,event.clientY - canvas.offsetTop);
        recover = context.getImageData(0, 0, canvas.width, canvas.height);
    }else if(draw_mode=="txt" && text!=""){
        context.strokeStyle = draw_color;
        context.font = `${sizestyle}px ${fontstyle}`;
        context.lineWidth = draw_width/5;
        context.strokeText(text, stroke_begin_x, stroke_begin_y);
        undo_array.push(context.getImageData(0, 0, canvas.width, canvas.height));
        undo_index+=1;
        redo_array = [];    
        redo_index = -1;
    }
    event.preventDefault();
}

function draw(event){
    if(is_drawing){
        context.strokeStyle = draw_color;
        context.lineWidth = draw_width;
        context.lineCap = "round";
        context.lineJoin = "round";
        if(draw_mode=="pen"){
            context.lineTo(event.clientX - canvas.offsetLeft,event.clientY - canvas.offsetTop);
            context.stroke();
        }else if(draw_mode=="triangle"){
            context.putImageData(recover, 0, 0);
            context.beginPath();
            context.lineTo((event.clientX - canvas.offsetLeft), (event.clientY - canvas.offsetTop));
            context.lineTo((event.clientX - canvas.offsetLeft)-(event.clientX - canvas.offsetLeft-stroke_begin_x)*2, (event.clientY - canvas.offsetTop));
            context.lineTo(stroke_begin_x, stroke_begin_y);
            context.stroke();
            context.closePath();
            context.stroke();
            context.closePath();
        }else if(draw_mode=="circle"){
            context.putImageData(recover, 0, 0);
            context.beginPath();
            dx = Math.abs((event.clientX - canvas.offsetLeft)-stroke_begin_x);
            dy = Math.abs((event.clientY - canvas.offsetTop)-stroke_begin_y);
            var dis = Math.sqrt(Math.pow(dx,2)+Math.pow(dy,2));
            context.arc(stroke_begin_x,stroke_begin_y, dis, 0, 2 * Math.PI);
            context.stroke();
            context.closePath();
        }else if(draw_mode=="square"){
            context.putImageData(recover, 0, 0);
            context.beginPath();
            context.rect(stroke_begin_x,stroke_begin_y,(event.clientX - canvas.offsetLeft)-stroke_begin_x,(event.clientY - canvas.offsetTop)-stroke_begin_y);
            context.stroke();
            context.closePath();
        }else if(draw_mode=="eraser"){
            context.clearRect(stroke_begin_x, stroke_begin_y, (event.clientX - canvas.offsetLeft)-stroke_begin_x, (event.clientY - canvas.offsetTop)-stroke_begin_y);
            context.fillStyle = canvas_color;//這裡因為背景和畫布顏色不一樣所以填回去
            context.fillRect(stroke_begin_x, stroke_begin_y, (event.clientX - canvas.offsetLeft)-stroke_begin_x, (event.clientY - canvas.offsetTop)-stroke_begin_y);
        }
    }
    event.preventDefault();
}

function stop(event){
    if(is_drawing){
        context.stroke();
        context.closePath();
        is_drawing = false;
        undo_array.push(context.getImageData(0, 0, canvas.width, canvas.height));
        undo_index+=1;
        redo_array = [];
        redo_index = -1;
    }
    event.preventDefault();
}

function change_color(element){
    draw_color = element.style.background;
}

function clear_canvas(){
    redo_array.push(context.getImageData(0, 0, canvas.width, canvas.height));
    redo_index += 1;
    undo_array = [];
    undo_index = -1;
    context.fillStyle = canvas_color;
    context.clearRect(0, 0, canvas.width, canvas.height);
    context.fillRect(0, 0, canvas.width, canvas.height);
}

function undo(){
    if(undo_index>0){
        redo_array.push(undo_array.pop());//會先pop掉當前狀態
        undo_index -= 1;
        redo_index += 1;
        context.putImageData(undo_array[undo_index], 0, 0);
    }else if(undo_index == 0){
        clear_canvas();//undo_array中沒有初始狀態，用clear_canvas代替
    }
}

function redo(){
    if(redo_index>-1){
        context.putImageData(redo_array[redo_index], 0, 0);
        undo_array.push(redo_array.pop());
        undo_index += 1;
        redo_index -= 1;
    }
}

function pen(){
    draw_mode="pen";
    canvas.style.cursor = `url("pen.png"),auto`;
}

function eraser(){
    draw_mode="eraser";
    canvas.style.cursor = `url("eraser.png"),auto`;
}

function triangle(){
    draw_mode="triangle";
    canvas.style.cursor = `url("triangle.png"),auto`;
}

function circle(){
    draw_mode="circle";
    canvas.style.cursor = `url("circle.png"),auto`;
}

function square(){
    draw_mode="square";
    canvas.style.cursor = `url("square.png"),auto`;
}

function txt(){
    draw_mode="txt";
    canvas.style.cursor = `url("txt.png"),auto`;
}

function click_img(){
    document.getElementById("img").click();
}

img.addEventListener('change', handleImage, false);

function handleImage(e){
    var realInput = document.getElementById("img");
    var reader = new FileReader();
    reader.onload = function(event){
        var img = new Image();
        img.src = event.target.result;
        img.onload = function(){
            context.drawImage(img,0,0);
            undo_array.push(context.getImageData(0, 0, canvas.width, canvas.height));
            undo_index+=1;
            redo_array = [];
            redo_index = -1;
        }
    }
    reader.readAsDataURL(e.target.files[0]);   
}

function download(){
    var canvas = document.getElementById("canvas");
    var image = canvas.toDataURL();
    var link = document.createElement('a');
    link.download = 'canvas.png';
    link.href = image;
    link.click();
}

# Software Studio 2021 Spring
## Assignment 01 Web Canvas

### How to use 

    網頁中央的空白處即為畫布，游標置於畫布上點擊左鍵拖曳即可繪畫

    畫布下方則為功能鍵，由左至右分別為:

    undo、redo、清空畫布、切換至繪筆、切換至象擦、切換至三角形筆刷、切換至圓型筆刷、切換至矩形筆刷、置換筆刷顏色
    
    自定義顏色、調整筆刷粗細、切換至文字方塊、文字方塊內容、文字方塊字體大小、文字方塊字型、上傳圖檔、下載畫布

### Function description

    文字方塊的字體通用筆刷的特性，包含顏色及線條粗細，一樣在畫布下方選取顏色、調整粗細即可

### Gitlab page link

    https://108062231.gitlab.io/AS_01_WebCanvas

### Others (Optional)

    還沒做完assignment1，assignment2就發布了嗚嗚嗚......

<style>
table th{
    width: 100%;
}
</style>